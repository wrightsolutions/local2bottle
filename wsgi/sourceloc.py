#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from bottle import route, default_app, Bottle
from os import path as ospath
from os import sep as ossep

try:
    import psycopg2 as psy
except ImportError:
    import psycopg as psy

conn = None
try:
    conn = psy.connect("dbname='sourceloc' user=sourceloc")
except psy.OperationalError,e:
    print "Connection to database failed:\n\t{0}".format(e.args[0])

cur = None
datestyle = None
cur = conn.cursor()
if cur:
    cur.execute('SHOW DateStyle')
    row = cur.fetchone()
    if row:
        datestyle = row[0]

root_greeting = 'Source Code Analysis'
if datestyle:
    root_greeting = "{0} {1}".format(root_greeting,datestyle)

def all_numbers(all_list,number_type='int'):
    """ In its simplest form, just give this function a list of numbers
    which you want int() tested. Then check the return value is not False.
    This function can do more, including giving you back the converted
    list (if required). """
    all_numbers_return = True
    all_list_return = []
    for item in all_list:
        try:
            item_as_number = int(item)
            all_list_return.append(item_as_number)
        except:
            all_numbers_return = False
    if all_numbers_return and len(all_list_return) == len(all_list):
        all_numbers_return = all_list_return
    return all_numbers_return

def html_body_wrapper(inner_markup):
    return "<html>\n<body>\n{0}\n</body>\n</html>".format(inner_markup)

def html_body_wrapper3(inner_inner_markup,highlight=None,notfound=False):
    """ 3 argument variant.
    notfound should only be set True when the first argument
    inner_inner_markup is a plain text string that requires TEXTUAL prepending. """

    if notfound:
        inner_inner_markup = "No programs found {0}".format(inner_inner_markup)
    if highlight is not None:
        if highlight in ['b','strong','i','em']:
            """ Read up on html5 <b> versus <strong> - neither is preferred
            outright and both have modern applications. """
            inner_markup = "<{0}>{1}</{0}>".format(highlight,inner_inner_markup)
        else:
            inner_markup = inner_inner_markup
    return html_body_wrapper(inner_markup)

def list_as_paragraphs(list_for_markup):
    return "\n".join(["<p>{0}</p>".format(item) for item in list_for_markup])

def list_as_paragraphs_sorted(list_for_markup):
    return list_as_paragraphs(sorted(list_for_markup))

def sml_filter(config):
    """ Matches 'small', 'medium, or 'large'. """
    list_acceptable = ['small','medium','large']
    case_indicator = config
    regexp = "({0})".format(chr(124).join(list_acceptable))
    
    def to_python(sml):
        if sml in list_acceptable:
            return sml
        return None

    def to_url(sml):
        return sml.lower()

    return regexp, to_python, to_url

def nssml_filter(config):
    """ Matches 'none', 'single', 'small', 'medium, or 'large'.
    nssml (abbreviation) is just the first letter of each acceptable value. """

    list_acceptable = ['none','single','small','medium','large']
    case_indicator = config
    regexp = "({0})".format(chr(124).join(list_acceptable))
    
    def to_python(nssml):
        if nssml in list_acceptable:
            return nssml
        return None

    def to_url(nssml):
        return nssml.lower()

    return regexp, to_python, to_url

def reconstruct_suitable_path_from_list_of_strings_worker(list_of_strings,
                                                          return_on_exception=ossep):
    filtered_list = [s for s in list_of_strings if s is not None]
    try:
        reconstructed = ospath.join(*filtered_list)
    except AttributeError,e:
        reconstructed = return_on_exception

    return reconstructed

def reconstruct_suitable_path_from_list_of_strings(list_of_strings):

    if len(list_of_strings) < 2:
        if list_of_strings is None:
            return ossep
        return list_of_strings[0]

    return reconstruct_suitable_path_from_list_of_strings_worker(list_of_strings)

def source_linecount_between(lower_lim,upper_lim):

    if not all_numbers([lower_lim,upper_lim]):
        return []

    rows = None
    if cur:
        select_query = "SELECT project,subdirpath,filename FROM source WHERE fileext = 'py'" \
            " AND linecount BETWEEN {0} AND {1};".format(lower_lim,upper_lim)
        cur.execute(select_query)
        rows = cur.fetchall()

    source_list = []
    if rows:
        for row in rows:
            res = reconstruct_suitable_path_from_list_of_strings([row[0],row[1],row[2]])
            source_list.append(res)

    return source_list


def source_defcount_between(lower_lim,upper_lim):

    if not all_numbers([lower_lim,upper_lim]):
        return []

    rows = None
    if cur:
        select_query = "SELECT project,subdirpath,filename FROM source WHERE fileext = 'py'" \
            " AND defcount BETWEEN {0} AND {1};".format(lower_lim,upper_lim)
        cur.execute(select_query)
        rows = cur.fetchall()

    source_list = []
    if rows:
        for row in rows:
            res = reconstruct_suitable_path_from_list_of_strings([row[0],row[1],row[2]])
            source_list.append(res)

    return source_list


def sourcecode_like(like_string):
    """ Do not rely on this function to provide wrapping percent symbols. 
    If you want to search for .asctime() then give a like string as follows:
    %.asctime()%
    Where you have provided the percent symbols (%) yourself as part of like_string.
    """

    if '%' not in like_string:
        return []
    if ';' in like_string:
        return []
        """ Disable this semi-colon block if you are sure of non abuse / injection """

    rows = None
    if cur:
        select_query = "SELECT project,subdirpath,filename FROM source WHERE fileext = 'py'" \
            " AND sourcecode LIKE '{0}';".format(like_string)
        #print select_query
        cur.execute(select_query)
        rows = cur.fetchall()

    source_list = []
    if rows:
        for row in rows:
            res = reconstruct_suitable_path_from_list_of_strings([row[0],row[1],row[2]])
            source_list.append(res)

    return source_list


app=Bottle()
app.router.add_filter('sml', sml_filter)
app.router.add_filter('nssml', nssml_filter)

@app.route('/comprehensions')    
def comprehensions():
    rows = None
    if cur:
        select_query = "SELECT project,subdirpath,filename FROM source WHERE fileext = 'py'" \
            " AND sourcecode LIKE '%[ % for % in % ]%';"
        cur.execute(select_query)
        rows = cur.fetchall()

    source_list = []
    if rows:
        for row in rows:
            res = reconstruct_suitable_path_from_list_of_strings([row[0],row[1],row[2]])
            source_list.append(res)

    if len(source_list) > 0:
        return html_body_wrapper(list_as_paragraphs_sorted(source_list))
    return '<strong>No list comprehensions found</strong>'


@app.route('/linecount/<size:sml>')
def linecount_sml(size):
    if size == 'small':
        lower_lim = 1
        upper_lim = 50
    elif size == 'medium':
        lower_lim = 51
        upper_lim = 150
    else:
        lower_lim = 151
        upper_lim = 999999

    source_list = source_linecount_between(lower_lim,upper_lim)
    if len(source_list) > 0:
        return html_body_wrapper(list_as_paragraphs_sorted(source_list))
    return '<strong>No programs of suitable size found.</strong>'


@app.route('/defcount/<num:int>')     # bottle >= 0.10
def defcount_exactly(num='0'):
    source_list = source_defcount_between(num,num)
    if len(source_list) > 0:
        return html_body_wrapper(list_as_paragraphs_sorted(source_list))
    return '<strong>No programs found with defcount given.</strong>'


@app.route('/defcount/<size:nssml>')
def defcount_sml(size):
    if size == 'none':
        lower_lim = 0
        upper_lim = 0
    elif size == 'single':
        lower_lim = 1
        upper_lim = 1
    elif size == 'small':
        lower_lim = 2
        upper_lim = 5
    elif size == 'medium':
        """ A program source file having between 6 and 15 functions is
        classified here as 'medium'. The limits are arbitrary and depending
        on your typical projects might be better adjusted by you to suit. """
        lower_lim = 6
        upper_lim = 15
    else:
        lower_lim = 16
        upper_lim = 999999

    source_list = source_defcount_between(lower_lim,upper_lim)
    if len(source_list) > 0:
        return html_body_wrapper(list_as_paragraphs_sorted(source_list))

    return html_body_wrapper3('with defcount of suitable size.','strong',True)

@app.route('/asctime')
def dot_asctime():

    source_list = sourcecode_like('%.asctime(%')

    if len(source_list) > 0:
        return html_body_wrapper(list_as_paragraphs_sorted(source_list))
    return html_body_wrapper3('using .asctime(','strong',True)


@app.route('/shlex')
def dot_asctime():

    source_list = sourcecode_like('%import shlex%')

    if len(source_list) > 0:
        return html_body_wrapper(list_as_paragraphs_sorted(source_list))
    return html_body_wrapper3('using import shlex','strong',True)


@app.route('/purge')
def dot_asctime():

    source_list = sourcecode_like('%--purge%')

    if len(source_list) > 0:
        return html_body_wrapper(list_as_paragraphs_sorted(source_list))
    return html_body_wrapper3('using --purge','strong',True)

 
@app.route('/')
def index():
    return '<b>%s</b>' % root_greeting


@app.route('/intruder')
def jresponse():
    return 'window'

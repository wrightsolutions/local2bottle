from sourceloc import app
import newrelic.agent
newrelic.agent.initialize('/etc/newrelic/nragentpython.cfg')
#application = app
application = newrelic.agent.wsgi_application()(app)
